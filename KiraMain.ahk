; [PURPOSE] Main file for running scripts


/*
[LICENSE] “Navigation” pack of Pixelmeetup:
https://www.flaticon.com/free-icon/loop_1635605
*/
Menu, Tray, Icon, %PATH_KIROMANIA%\KiraIcons\KiraMain.ico


; [PURPOSE] Template for specific types of streams
KiraStreamType(KiraType)
{
	Run %PATH_KIROMANIA%\KiraBeforeStream\Kira%KiraType%\Kira%KiraType%Before.ahk
	OutputDebug Run %PATH_KIROMANIA%\KiraBeforeStream\Kira%KiraType%\Kira%KiraType%Before.ahk
}


/*
[ACTION] Run scripts for any stream

[INFO] “CapsLock” as modifier key:
[LINK] KiraStream\KiraWhatWhereWhen\KiraHotkeys.ahk:12
*/
CapsLock & F1::
Run %PATH_KIROMANIA%\KiraBeforeStream\KiraBeforeAll.ahk
OutputDebug Run %PATH_KIROMANIA%\KiraBeforeStream\KiraBeforeAll.ahk
return

; [ACTION] Run scripts after any stream
CapsLock & F4::
Run %PATH_KIROMANIA%\KiraAfterStream\KiraAfterAll.ahk
OutputDebug Run %PATH_KIROMANIA%\KiraAfterStream\KiraAfterAll.ahk
return

; [ACTION] “CapsLock + T” — run Triviador Russia
CapsLock & sc21::
Run %PATH_KIROMANIA%\KiraBeforeStream\KiraTriviador\KiraTriviadorRussiaBefore.ahk
OutputDebug Run %PATH_KIROMANIA%\KiraBeforeStream\KiraTriviador\KiraTriviadorRussiaBefore.ahk
return

; [ACTION] “CapsLock + W” — run scripts for “What? Where? When?”
CapsLock & sc11::
KiraStreamType("WhatWhereWhen")
; [REQUIRED] “return” here, not in the template
return

; [ACTION] “CapsLock + V” — run ViDi-DC
CapsLock & sc2f::
KiraStreamType("ViDiDC")
return
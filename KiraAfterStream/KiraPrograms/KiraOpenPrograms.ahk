﻿; [ACTION] Open programs for everyday usage after stream


/*
[LEARN][AUTOHOTKEY] Get “ProgramFiles(x86)” environment variable:
https://www.autohotkey.com/boards/viewtopic.php?t=49837#p221101

[NOTE] Environment variable “%ProgramFiles(x86)%” not work in AutoHotkey
*/
EnvGet, KiraProgramFilesX86, ProgramFiles(x86)


/*
[ACTION] Open Xtreme Download Manager (XDM)

[INFO] I get command below into ProcessHacker, put cursor to “javaw.exe”

[REQUIRED] Double quotes in "%KiraProgramFilesX86%\XDM\xdman.jar"

[WARNING] I don't know, how I can add XDM to “KiraList”;
path after “javaw -jar” required no spaces
https://stackoverflow.com/a/24796528/5951529
*/
KiraXDMCommand = %KiraProgramFilesX86%\XDM\java-runtime\bin\javaw.exe -jar "%KiraProgramFilesX86%\XDM\xdman.jar"

Run %KiraXDMCommand%
OutputDebug Run %KiraXDMCommand%


/*
[ACTION] Open Ferdi

[WARNING] If you open Ferdi from Scoop,
you get command prompt with Node.js errors:
*/


/*
[ACTION] Open Thunderbird

[INFO] I use Thunderbird from PortableApps:
https://portableapps.com/apps/internet/thunderbird_portable
*/


/*
[ACTION] Open ConEmu

[LEARN][CONEMU] “-run” command-line argument needs for open single window in ConEmu + Far:
https://conemu.github.io/en/ConEmuArgs.html#conemuexe-command-line-switches
*/


KiraList := ["ferdi", PATH_PORTABLEAPPS . "\PortableApps\ThunderbirdPortable\ThunderbirdPortable.exe", "conemu64 -run far -Dir D:\SashaDebugging"]

KiraLoop("KiraOpenApps", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
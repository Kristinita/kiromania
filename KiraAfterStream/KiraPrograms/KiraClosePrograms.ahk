﻿; [ACTION] Close streaming-specific apps and AutoHotkey scripts


/*
[REQUIRED] “DetectHiddenWindows” in this file:
[LINK] ..\..\KiraTemplates\KiraTemplateCloseAllTabs.ahk:11
*/
DetectHiddenWindows 1

/*
[REQUIRED] “SetTitleMatchMode” in this file
[LINK] ..\..\KiraStream\KiraSwitchScenes.ahk:24
*/
SetTitleMatchMode 2


/*
[ACTION] Close ConEmu
[LINK] ..\..\KiraBeforeStream\KiraPrograms\KiraClosePrograms.ahk:12

[INFO] Close ConEmu primarily, so that
it would be closed faster than reopen with another parameters for everyday usage
*/
KiraKillAppAndTree("ConEmu64")


/*
[ACTION] Close PhantomBot window

[INFO] My default console app is ConEmu + Far, not cmd;
I can't have another “cmd”, so I can kill cmd

[NOTE] PhantomBot have “cmd” + “conhost” + “java” processes
“conhost” may be used in Far Manager;
if we close solely “cmd” or “java”, PhantomBot window will not close
*/
KiraKillAppAndTree("cmd")


/*
[ACTION] Close Firefox window, that created by “Windowed”
or “New window without toolbars” addons
*/
WinClose ahk_class MozillaDialogClass
OutputDebug Close Firefox MozillaDialogClass window


; [INFO] List of apps for closing
KiraAppsList := ["obs64", "WinSize2", "Restream Chat", "ViDi-DC", "chrome"]

; [INFO] List of scripts for closing
KiraScriptsList := ["KiraSwitchScenes", "KiraHotkeys"]


KiraLoop("KiraCloseNonStreamProcesses", KiraAppsList)

KiraLoop("KiraCloseAHKScripts", KiraScriptsList)


#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateKillTree.ahk

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
﻿/*
[ACTION] Switch from “KiraStreaming” — my Sublime Text project for streaming —
to my default “SashaProject” project
*/

KiraSwitchSublimeTextProjects("SashaProject", "KiraStreaming")

#Include %A_ScriptDir%\..\..\KiraGeneral\KiraSublimeText\KiraSwitchProjects.ahk

return
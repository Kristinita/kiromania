; [ACTION] Set Sublime Text after streaming

KiraList := ["KiraAfterStream\KiraSublimeText\KiraProjectSashaProject", "KiraGeneral\KiraSublimeText\KiraRevertFontSize"]

KiraLoop("KiraRunAHKScripts", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
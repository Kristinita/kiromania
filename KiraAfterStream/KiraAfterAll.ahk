; [ACTION] Run commands, required for every stream, after stream

KiraList := ["KiraAfterStream\KiraPrograms\KiraClosePrograms", "KiraAfterStream\KiraPrograms\KiraOpenPrograms", "KiraAfterStream\KiraSublimeText\KiraSublimeTextAfter", "KiraAfterStream\KiraFirefox\KiraFirefoxAfter"]

KiraLoop("KiraRunAHKScripts", KiraList)

#Include %A_ScriptDir%\..\KiraTemplates\KiraTemplateLoop.ahk
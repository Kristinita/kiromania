; [ACTION] Open tabs for everyday usage after stream

/*
[INFO] Stack Overflow on Russian review list:
[REQUIRED] Review Stalker Reloaded —
tool by which possible get messages for review automatically:
https://stackapps.com/q/6869/42078

[INFO] Kristinita's Search main page

[INFO] Google main page

[INFO] Page for editing Kristinita's Search resources
*/

KiraList := ["ru.stackoverflow.com/review", "kristinita.netlify.app", "google.com", "cse.google.com/cse/setup/basic?cx=013024336414733191742%3Asps98skj394"]

KiraLoop("KiraRunWebPages", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
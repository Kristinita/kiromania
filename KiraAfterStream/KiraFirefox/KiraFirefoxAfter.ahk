; [ACTION] Set Firefox after streaming for everyday usage

KiraTabs = "KiraAfterStream\KiraFirefox\KiraFirefoxEverydayTabs"

/*
[LINK] Pass variable across scripts:
..\..\KiraGeneral\KiraFirefox\KiraCloseAllTabs.ahk:17
*/
Run %PATH_KIROMANIA%\KiraGeneral\KiraFirefox\KiraCloseThenOpenTabs.ahk %KiraTabs%

return
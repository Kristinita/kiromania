/*
[DEPRECATED] If Advanced Scene Switcher correctly configured,
OBS Studio show expected scene if windows switched by any method:
By hotkey, “Alt+Tab” or mouse clicks in taskbar.
https://obsproject.com/forum/resources/advanced-scene-switcher.395/
https://github.com/WarmUpTill/SceneSwitcher/issues/26


[ACTION] Disable “Alt+Tab” hotkey for streaming

[PURPOSE] In the stream I can switch between windows as usual,
use shortcut “Alt+Tab”
But scenes in OBS Studio will not switch in this case.
Switching via specific shortcuts from “KiraSwitchScenes.ahk” prevented it.
*/

/*
[LICENSE] “SEO and online marketing” pack of Freepik:
https://www.flaticon.com/free-icon/browsers_202383?term=tab&page=1&position=57
*/
Menu, Tray, Icon, %PATH_KIROMANIA%\KiraIcons\KiraWindows.ico

/*
[INFO] Disable Alt+Tab
https://superuser.com/a/559052/572069
*/
!Tab::return
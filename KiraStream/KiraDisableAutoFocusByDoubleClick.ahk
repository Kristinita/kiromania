/*
[DEPRECATED] If Advanced Scene Switcher correctly configured,
OBS Studio will automatically switched to correct scenes.


[ACTION] Stay focused on File Explorer, when open any file by double-click

[PURPOSE] Double-click in File Explorer open another program,
that doesn't display for stream viewers.
This script return focus to File Explorer, that I focus to program, use hotkey
*/

/*
[LICENSE] “Business” pack of Freepik:
https://www.flaticon.com/free-icon/folder_2786480
*/
Menu, Tray, Icon, %PATH_KIROMANIA%\KiraIcons\KiraFolder.ico


; [REQUIRED] “:”
KiraFileExplorerTitle := "ahk_class CabinetWClass ahk_exe explorer.exe"

/*
[LEARN][AUTOHOTKEY] “#IfWinActive”:
https://www.autohotkey.com/docs/commands/_IfWinActive.htm

[LEARN][AUTOHOTKEY] Difference between “#IfWinActive” and “if WinActive”:
https://www.autohotkey.com/docs/commands/WinActive.htm
https://autohotkey.com/board/topic/78534-difference-between-ifwinactive-and-ifwinactive/

[LEARN][AUTOHOTKEY] Using variables for “#IfWinActive”,
normal variables usage doesn't work in these case:
https://autohotkey.com/board/topic/75461-ifwinactive-does-not-accept-a-variable-for-a-title-name/?p=480265
*/
#If WinActive(KiraFileExplorerTitle)

/*
[LEARN][AUTOHOTKEY] Double click:
https://autohotkey.com/board/topic/23224-resolved-catch-a-double-press-click/?p=150299
*/
~LButton::

/*
[LEARN][AUTOHOTKEY] “KeyWait”:
https://www.autohotkey.com/docs/commands/KeyWait.htm

[INFO] Wait for LButton to be released
*/
KeyWait LButton

/*
[INFO] Pressed again within 0.3 seconds

[NOTREQUIRED] Leading zero before “.3”
*/
KeyWait, LButton, D T.3

/*
[LEARN][AUTOHOTKEY] “ErrorLevel”:
https://www.autohotkey.com/docs/misc/ErrorLevel.htm

[INFO] Nothing occurs, if single left click
*/
if ErrorLevel
	return

else

	/*
	[NOTE] SumatraPDF can open big books files slowly;
	if I decrease sleep value, script may works incorrectly
	*/
	Sleep 2400

	/*
	[FIXME][BUG] Doesn't work

	[LEARN][AUTOHOTKEY] “IfWinNotActive”:
	https://www.autohotkey.com/docs/commands/_IfWinActive.htm

	[INFO] If after double-click not File Explorer window focused,
	scirpt make File Explorer focused

	[LEARN][AUTOHOTKEY] “IfWinNotActive” deprecated:
	https://www.autohotkey.com/docs/commands/WinActive.htm#command

	#If WinActive(,,KiraFileExplorerTitle)
	*/

	WinActivate %KiraFileExplorerTitle%

	/*
	[LEARN][AUTOHOTKEY] “WinGetTitle” — get title:
	https://www.autohotkey.com/docs/commands/WinGetTitle.htm

	[NOTE] I get current path of File Explorer,
	not the title of opened program
	*/
	WinGetTitle KiraTitle

	OutputDebug
	(
	After double-click in “%KiraTitle%” folder
	focus switched from File Explorer to another app.

	Now focus switched to File Explorer back
	)

return
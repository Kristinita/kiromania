/*
[ACTION] Switching to specific scenes in OBS Studio

[PURPOSE] If streamer will switch, use Alt+Tab, viewers will not see windows;
So I set specific hotkey for activating each window
*/


/*
[LEARN][AUTOHOTKEY] Set custom tray icon:
https://autohotkey.com/board/topic/59084-how-to-change-default-ahk-file-icon/?p=482046
https://www.autohotkey.com/docs/commands/Menu.htm#TrayIcon
[NOTE] Relative paths required; “D:\AutoHotkey\Icons\KiraSwitch.ico” will not work

[LICENSE] “Essentials” pack of Smashicons:
https://www.flaticon.com/free-icon/switch_2489111?term=switch&page=3&position=67

[NOTE] If size of original icon is smaller, quality in tray is better.
*/
Menu, Tray, Icon, %PATH_KIROMANIA%\KiraIcons\KiraSwitch.ico


/*
[REQUIRED] For activating any window, that contains “EricsBooks” in title:
https://www.autohotkey.com/docs/commands/SetTitleMatchMode.htm
https://autohotkey.com/board/topic/57375-activate-window-on-title-with-wildcard/?p=360413

[REQUIRED] “SetTitleMatchMode” in “KiraSwitchScenes”,
not in “KiraTemplateSwitchScenes”
*/
SetTitleMatchMode 2


/*
[INFO] Ctrl+Alt+V — Activate ViDi-DC and Restream Chat
[INFO] “ViDi-DC” in first, because no “Window Capture”
OBS method for ViDi-DC:
https://github.com/obsproject/obs-studio/issues/2554
Because of this unexpected display parts can be added to the stream
*/
^!v::
KiraSwitchToScene("ViDi-DC", "Tclientform", true)
return

; [INFO] Ctrl+Alt+F — Activate Firefox
^!f::
KiraSwitchToScene("firefox", "MozillaWindowClass", true)
return

; [INFO] Ctrl+Alt+S — Activate SumatraPDF
^!s::
KiraSwitchToScene("SumatraPDF", "SUMATRA_PDF_FRAME", true)
return

; [INFO] Ctrl+Alt+C — Activate ConEmu
^!c::
KiraSwitchToScene("ConEmu64", "VirtualConsoleClass", true)
return

; [INFO] Ctrl+Alt+T — Activate Tablacus Explorer (TE.exe)
^!t::
KiraSwitchToScene("TE", "TablacusExplorer", true)
return

/*
[DEPRECATED] I switched to Tablacus Explorer from Windows Explorer

[INFO] Ctrl+Alt+E — Activate EricsBooks folder

[REQUIRED] Registry changes, that full path will shown in “explorer.exe”:
https://winaero.com/blog/full-path-title-explorer-windows-10/

[INFO] “EricsBooks” is folder; “KiraFolder” parameter required

[INFO] Hide Navigation Pane in File Explorer via Ribbon UI:
https://winaero.com/blog/disable-navigation-pane-windows-10/

*^!e::
KiraSwitchToScene("EricsBooks",,true)
return
*/

; [INFO] Ctrl+Alt+N — Activate Sublime Text (N — Notes)
^!n::
KiraSwitchToScene("sublime_text", "PX_WINDOW_CLASS")
return

/*
[INFO] Ctrl+Alt+I — Activate ImageGlass

[INFO] For ImageGlass on the streams:
1. Disable Toolbar — “T”
2. Disable titlebar (Frameless) — “F10”
*/
^!i::
KiraSwitchToScene("ImageGlass", "WindowsForms10.Window.8.app.0.141b42a_r6_ad1", true)
return

/*
[INFO] Ctrl+Alt+D — Activate Triviador

[REQUIRED] “New Window without toolbar” addon:
https://addons.mozilla.org/en-US/firefox/addon/new-window-without-toolbar/
It open window with ahk_class “MozillaDialogClass”
*/
^!d::
KiraSwitchToScene("firefox", "MozillaDialogClass", true)
return

/*
[INFO] Ctrl+Alt+W — Activate Web Speech API in Chrome
*/
^!w::
KiraSwitchToScene("chrome", "Chrome_WidgetWin_1", true)
return

/*
[INFO] Ctrl+Alt+L — Activate LibreOffice

[INFO] LibreOffice exe file is “swriter.exe”, but “ahk_exe” for LibreOffice is “soffice.bin”

[WARNING] LibreOffice doesn't support multiple docs as tabs instead of separate windows:
https://ask.libreoffice.org/en/question/192261/writer-open-multiple-docs-as-tabs-instead-of-separate-windows/
*/
^!l::
KiraSwitchToScene("soffice", "SALFRAME", true, "bin")
return


/*
[LEARN][AUTOHOTKEY] “#Include” — share functions across file:
https://www.autohotkey.com/docs/commands/_Include.htm
[INFO] “#Include” must be after functions,
not in the file beginning
*/
#Include %A_ScriptDir%\..\KiraTemplates\KiraTemplateSwitchScenes.ahk
/*
[PURPOSE] Toggle WebSpeechAPI Speech to Text

[ACTION] Click to Microphone button on this page and switch to Firefox:
https://www.google.com/intl/en/chrome/demos/speech.html
*/


/*
[REQUIRED] Surfingkeys Chromium addon:
https://chrome.google.com/webstore/detail/surfingkeys/gfbliohnnapiefjpjlpjnehglfpaknnc
*/
WinActivate ahk_class Chrome_WidgetWin_1 ahk_exe chrome.exe
Send f
Send a
OutputDebug Toggle Speech-To-Text


; [INFO] Activate Firefox, that would be possible quickly make YouTube player actions
WinActivate ahk_class MozillaDialogClass ahk_exe firefox.exe

return
; [PURPOSE] Hotkeys for “What? Where? When?”


/*
[LICENSE] “Halloween” pack of Freepik:
https://www.flaticon.com/free-icon/owl_2213682?term=owl&page=1&position=36
*/
Menu, Tray, Icon, %PATH_KIROMANIA%\KiraIcons\KiraOwl.ico


/*
[LEARN][AUTOHOTKEY] Use CapsLock as modifier key:
https://superuser.com/a/1358752/572069

[REQUIRED] Scancodes instead of keys
for working in different keyboard layouts

[LEARN][AUTOHOTKEY] Automatically get scancodes:
https://www.autohotkey.com/boards/viewtopic.php?t=24173#p114367
*/


; [INFO] CapsLock + m — microphone
CapsLock & sc32::
Run %PATH_KIROMANIA%\KiraStream\KiraWhatWhereWhen\KiraToggleSpeechToText.ahk
OutputDebug Run %PATH_KIROMANIA%\KiraStream\KiraWhatWhereWhen\KiraToggleSpeechToText.ahk
return


; [INFO] CapsLock + b — 20 seconds — for blitz and Brain Ring
CapsLock & sc30::
KiraCHGKTimer("blitz")
return


; [INFO] CapsLock + d — 30 seconds — for doublets
CapsLock & sc22::
KiraCHGKTimer("doublet")
return


; [INFO] 1 minute — for CHGK
CapsLock & 1::
KiraCHGKTimer("chgk")
return


; [INFO] 2 minutes — for deep CHGK
CapsLock & 2::
KiraCHGKTimer("two_minutes")
return


/*
[LINK] “#Include”:
..\KiraSwitchScenes.ahk:131
*/
#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateCHGKTimer.ahk
; [ACTION] Open ViDi-DC notes

/*
[INFO] “Filimania”, because usually I play to this game.
But via ViDi-DC is possible play to “Alfahub” or “Khimki”.
*/
KiraList := ["Filimania"]

KiraLoop("KiraOpenNotes", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return

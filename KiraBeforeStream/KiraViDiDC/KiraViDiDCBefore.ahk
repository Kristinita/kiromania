; [ACTION] Run scripts for any streams, where streamers play in ViDi-DC

KiraList := ["KiraBeforeStream\KiraViDiDC\KiraViDiDCRun", "KiraBeforeStream\KiraViDiDC\KiraViDiDCNotes"]

KiraLoop("KiraRunAHKScripts", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
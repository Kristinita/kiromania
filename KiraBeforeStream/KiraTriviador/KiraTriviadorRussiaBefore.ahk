; [ACTION] Run Triviador Russia

/*
[WARNING] Triviador users must manually activate Flash player for every seance;
“Always Activate” option was disabled in Firefox 69:
https://bugzilla.mozilla.org/show_bug.cgi?id=1519434#c1
https://support.digication.com/hc/en-us/articles/115003926487-Enabling-Flash-for-Mozilla-Firefox-Windows-Macintosh-

[INFO] Put “%PATH_KIROMANIA%\KiraBeforeStream\KiraTriviador\KiraTriviadorRussia” to the beginning and
“%PATH_KIROMANIA%\KiraGeneral\KiraFirefox\KiraOpenURLInNewWindow.ahk” to the end of the list,
so that new Firefox window will not a blank
*/
KiraList := ["KiraBeforeStream\KiraTriviador\KiraTriviadorRussia", "KiraBeforeStream\KiraTriviador\KiraTriviadorNotes", "KiraGeneral\KiraFirefox\KiraOpenURLInNewWindow"]

KiraLoop("KiraRunAHKScripts", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
; [ACTION] Run scripts, required for every stream


/*
[LEARN][AUTOHOTKEY] “:=” stores expression result as a variable:
https://www.autohotkey.com/docs/commands/SetExpression.htm
*/

KiraList := ["KiraBeforeStream\KiraPrograms\KiraClosePrograms", "KiraBeforeStream\KiraPrograms\KiraOpenPrograms", "KiraStream\KiraSwitchScenes", "KiraBeforeStream\KiraSublimeText\KiraSublimeTextBefore", "KiraBeforeStream\KiraFirefox\KiraFirefoxBefore"]

KiraLoop("KiraRunAHKScripts", KiraList)


/*
[NOTE][AUTOHOTKEY] “PATH_KIROMANIA” variable will not work here.
AutoHotkey doesn't support variables in “#Include”
except “A_ScriptDir” and some others:
https://www.autohotkey.com/docs/commands/_Include.htm#Parameters
https://autohotkey.com/board/topic/93133-why-does-include-not-work-for-variables-is-there-a-better-option/
https://autohotkey.com/board/topic/33264-include-with-variables/

[LEARN][AUTOHOTKEY] “A_ScriptDir” — the full path of the directory where the current script is located:
https://www.autohotkey.com/docs/Variables.htm#ScriptDir

[NOTE] Use “A_ScriptDir” in scripts instead of relative paths.
If no, relative paths will be counted from “KiraMain.ahk”
and you will have these problems:
1. You can't debugging specific scripts without changing path after “#Include”.
2. You can't quickly navigate between your script use tools as OpenURL

[LEARN][OPENURL] For navigation between scripts with “A_ScriptDir” variable
add to your “User/open_url.sublime-settings” file these lines:
```
"aliases": {
	"%A_ScriptDir%": "."
},
```
https://github.com/noahcoad/open-url#settings
https://github.com/noahcoad/open-url/issues/49
*/

#Include %A_ScriptDir%\..\KiraTemplates\KiraTemplateLoop.ahk
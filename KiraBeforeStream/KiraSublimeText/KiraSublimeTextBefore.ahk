; [ACTION] Set Sublime Text for streaming


/*
[INFO] Stop Pomodoro actions
Run %PATH_KIROMANIA%\KiraBeforeStream\KiraSublimeText\KiraStopPomodoro.ahk
*/
KiraList := ["KiraBeforeStream\KiraSublimeText\KiraProjectKiraStreaming", "KiraGeneral\KiraSublimeText\KiraRevertFontSize", "KiraBeforeStream\KiraSublimeText\KiraCloseAllTabs"]

KiraLoop("KiraRunAHKScripts", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return

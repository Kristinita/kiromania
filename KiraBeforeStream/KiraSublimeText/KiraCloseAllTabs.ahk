/*
[ACTION] Close all tabs, but not a Sublime Text window
https://stackoverflow.com/a/13561797/5951529
*/
Run subl --command close_all

return
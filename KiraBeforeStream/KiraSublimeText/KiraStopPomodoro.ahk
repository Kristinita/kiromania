﻿/*
[DEPRECATED]
SubNotify (part of Sublime Pomodoro) supports
Growl for Windows and default Windows notifications.

1. Growl for Windows is outdated/
2. CairoShell doesn't support default Windows notifications.
https://github.com/facelessuser/SubNotify/issues/9

I use “Goodtime” FDroid package as Pomodoro timer


[ACTION] Stop Sublime Text Pomodoro timer
*/


/*
[INFO]
1. Pomodoro takes a place in the status bar
2. Pomodoro notifications — extra sounds on the stream
*/


/*
[REQUIRED] Pomodoro Sublime Text plugin:
https://packagecontrol.io/packages/Pomodoro
*/
Run subl --command pomodoro

return

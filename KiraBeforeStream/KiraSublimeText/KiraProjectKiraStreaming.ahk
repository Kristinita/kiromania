﻿/*
[ACTION] Switch from “SashaProject” — my default Sublime Text project —
to “KiraStreaming” — project for streams
*/
KiraSwitchSublimeTextProjects("KiraStreaming", "SashaProject")

#Include %A_ScriptDir%\..\..\KiraGeneral\KiraSublimeText\KiraSwitchProjects.ahk

return

﻿; [ACTION] Set Firefox for streaming


/*
[INFO] Hide bookmarks folders names
Run %A_ScriptDir%\..\..\KiraGeneral\KiraFirefox\KiraToggleFoldersNames.ahk
*/
KiraTabs = "KiraBeforeStream\KiraFirefox\KiraFirefoxStreamingTabs"


/*
[LINK] Pass variable across scripts:
..\..\KiraGeneral\KiraFirefox\KiraCloseAllTabs.ahk:17
*/
Run %PATH_KIROMANIA%\KiraGeneral\KiraFirefox\KiraCloseThenOpenTabs.ahk %KiraTabs%

return

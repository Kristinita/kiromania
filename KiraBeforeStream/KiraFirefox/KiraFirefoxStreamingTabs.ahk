; [ACTION] Open Firefox tabs, that required for Kiromania streaming


/*
[REQUIRED] AutoPinTab Firefox addon for automatically pinned these tabs:
https://coolsoft.altervista.org/en/autopintab
*/


/*
[INFO] Kristinita's Search, Google, Z-Library fulltext search,
PhantomBot web panel in default port, Twitch Mod View

[INFO] Shortcut keys for Google Search it would be nice:
https://addons.mozilla.org/en-US/firefox/addon/shortcut-keys-for-google-se/

[INFO] ZLibrary fulltext search:
https://b-ok.cc/blog/19

[INFO] PhantomBot web panel in default port
[NOTE] Manual PhantomBot restart may be required;
are possible long PhantomBot start delays, example:

```
[07-10-2020 @ 12:08:08.119 GMT] Developers: PhantomIndex, Kojitsari, ScaniaTV, Zackery (Zelakto) & IllusionaryOne
[07-10-2020 @ 12:08:08.119 GMT] https://phantombot.tv/
[07-10-2020 @ 12:08:08.119 GMT]
[07-10-2020 @ 12:08:32.820 GMT]
[07-10-2020 @ 12:08:33.414 GMT] Validated Twitch CHAT (oauth) OAUTH Token.
[07-10-2020 @ 12:08:33.569 GMT] Validated Twitch API (apioauth) OAUTH Token.
```

[REQUIRED] AutoAuth Firefox extension, so that the streamer
doesn't need to print PhantomBot
login and password each time:
https://addons.mozilla.org/en-US/firefox/addon/autoauth/

[INFO] Twitch Mod View:
https://help.twitch.tv/s/article/mod-view?language=ru
*/

KiraList := ["kristinita.netlify.app", "google.com", "b-ok.cc/fulltext", "twitch.tv/moderator/ornithopteraalexandrae", "localhost:25000/panel"]

KiraLoop("KiraRunWebPages", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
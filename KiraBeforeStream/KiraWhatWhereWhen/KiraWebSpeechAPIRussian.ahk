; [PURPOSE] “What? Where? When?” speech to text


/*
[NOTE] I couldn't find a good Web Speech API alternative:

[NOTE] Engines, that support Python SpeechRecognition package, is paid or have serious limits:
https://github.com/Uberi/speech_recognition/issues/480

[NOTE] PocketSphinx have bad results for real human speech:
https://github.com/cmusphinx/pocketsphinx/issues/213

[NOTE] Vosk have good recognition, but I don’t know, how I can pause/resume Speech-to-Text:
https://github.com/alphacep/vosk-api/issues/162
*/


/*
[ACTION] Run Web Speech API demo page

[REQUIRED] Chromium, not Firefox for Web Speech API:
https://caniuse.com/#feat=speech-recognition
https://platform-status.mozilla.org/#webspeech-recognition

[LEARN][CHROMIUM] Command-line flags:
https://superuser.com/a/545040/572069

[LEARN][CHROMIUM] Application mode — run Chromium without bars:
https://peter.sh/experiments/chromium-command-line-switches/#app
https://technastic.com/open-websites-in-application-mode-google-chrome/

[LEARN][CHROMIUM] Starting Chromium in application mode via command-line:
https://superuser.com/a/1421401/572069

[INFO] Difference between kiosk and application modes:
https://stackoverflow.com/questions/19229257#comment28460467_19229257

[BUG] Google demo page have an error:
“Pусский” with latin “P” instead of “Русский” with Cyrillic “P”:
https://issuetracker.google.com/issues/157816262
*/
Run chrome --app=https://google.com/intl/en/chrome/demos/speech.html --new-window
OutputDebug Open Web Speech API demonstration page in Chromium


/*
[REQUIRED] Tampermonkey (or Greasemonkey, Violentmonkey)
for automatically select Russian language in dropdown menu:

[LEARN][JQUERY] JQuery in Tampermonkey:
https://stackoverflow.com/a/25172740/5951529

[LEARN][JQUERY] Select specific item in dropdown menu:
https://stackoverflow.com/a/20753994/5951529

[LEARN][JQUERY] “change()” event after selecting the value:
https://stackoverflow.com/a/33252307/5951529


[INFO] Tampermonkey source

[INFO] "52" — the value of Russian language

```javascript
$(function () {
    $('select option[value="52"]').prop('selected', true).change();
});
```

*/

return

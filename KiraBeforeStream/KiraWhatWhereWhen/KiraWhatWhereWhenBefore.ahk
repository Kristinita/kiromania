; [ACTION] Run scripts for “What? Where? When?” streams


KiraList := ["KiraBeforeStream\KiraWhatWhereWhen\KiraWebSpeechAPIRussian", "KiraBeforeStream\KiraWhatWhereWhen\KiraWhatWhereWhenNotes", "KiraStream\KiraWhatWhereWhen\KiraHotkeys"]

KiraLoop("KiraRunAHKScripts", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
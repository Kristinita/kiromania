﻿; [PURPOSE] Open programs and folders for streaming


/*
[ACTION] Open PhantomBot

[REQUIRED] Path to “AutoHotkey.exe” not to “AutoHotkey32.exe” in SublimeAutoHotkey “sublime-settings” file:
https://github.com/ahkscript/SublimeAutoHotkey/blob/master/AutoHotkey.sublime-settings#L7

[WARNING] Chocolatey not generate folder “current” as Scoop;
users needs change “PhantomBot-{version}” folder after each new version.
*/

/*
[ACTION] Open WinSize2:
https://superuser.com/a/240375/572069
https://sourceforge.net/projects/winsize2/

[NOTE] Currently, not added to Chocolatey:
https://github.com/chocolatey-community/chocolatey-package-requests/issues/799
*/

/*
[ACTION] Open Restream Chat

[LEARN][AUTOHOTKEY] Variables:
https://www.autohotkey.com/docs/Variables.htm#Intro

[LEARN][WINDOWS] “LOCALAPPDATA”:
https://renenyffenegger.ch/notes/Windows/dirs/Users/username/AppData/Local/index

[LEARN][AUTOHOTKEY] Use dot for concatenate (join) strings and variables:
https://www.autohotkey.com/docs/Variables.htm#Intro
*/

/*
[ACTION] Open blank SumatraPDF

[LEARN][SUMATRAPDF] Command-line arguments:
https://www.sumatrapdfreader.org/docs/Command-line-arguments.html

[LEARN][SUMATRAPDF] Settings:
https://www.sumatrapdfreader.org/settings/settings.html

[REQUIRED] “RestoreSession = false” in SumatraPDF settings, that all previous tabs will be closed:
https://github.com/sumatrapdfreader/sumatrapdf/issues/388

[NOTE] If “-restrict” or “-new-window” options, previous tabs still opened:
https://www.sumatrapdfreader.org/docs/Command-line-arguments.html

[NOTE] “Close other tabs” feature not exists:

[WARNING] Extra blank window “shims\SumatraPDF.exe” will show after SumatraPDF run:
https://github.com/lukesampson/scoop/issues/1606

[INFO] Put PhantomBot to the beginning of the list,
so that PhantomBot Firefox panel (https://localhost:25000/panel) will open succesfully

[INFO] Put SumatraPDF, Tablacus Explorer and ConEmu to the end,
so that these apps will close quickly than reopen with another parameters
*/
KiraList := [ChocolateyInstall . "\lib\phantombot\tools\PhantomBot-3.1.2\launch.bat", "D:\SashaPrograms\WinSize2\WinSize2.exe", LOCALAPPDATA . "\Programs\restream-chat\Restream Chat.exe", "SumatraPDF"]

KiraLoop("KiraOpenApps", KiraList)


/*
[ACTION] Open OBS Studio

[REQUIRED] Setting working directory

[LEARN][AUTOHOTKEY] “SetWorkingDir”:
https://www.autohotkey.com/docs/commands/SetWorkingDir.htm

[LEARN][AUTOHOTKEY] “SetWorkingDir” may be used as “Run” parameter:
https://www.autohotkey.com/docs/commands/Run.htm#Parameters
https://autohotkey.com/board/topic/145966-how-to-run-a-bat-working-file-from-ahk-really-so-simple/?p=725665

[LEARN][WINDOWS] ProgramFiles:
https://www.computerhope.com/jargon/p/progfile.htm

[LEARN][AUTOHOTKEY] Environment variables “C:\Program Files (x86)” and “C:\Program Files” for 64-bit Windows:
https://www.autohotkey.com/docs/Variables.htm#os
*/
KiraOBSStudioFolder = %ProgramW6432%\obs-studio\bin\64bit\
KiraOBSStudioPath = %KiraOBSStudioFolder%obs64.exe

Run, %KiraOBSStudioPath%, %KiraOBSStudioFolder%
OutputDebug Run %KiraOBSStudioPath%


/*
[ACTION] Open “KiraTriviador” and “EricsBooks” tabs in Tablacus Explorer

[LEARN][TABLACUS_EXPLORER] Command-line usage:
https://github.com/tablacus/TablacusExplorerAddons/issues/25#issuecomment-371307357
Any command may be run after Tablacus Explorer start:

[LEARN][TABLACUS_EXPLORER] You can find list of all available commands:
“Tools” → “Add-ons…” → “Key” → “Type”:
https://i.imgur.com/4S8nVxv.png

[INFO] I open “KiraTriviador” → close other tabs → open “EricsBooks”

[INFO] "Add-ons,Tree" also available, if you use “Tree View” addon:
https://tablacus.github.io/wiki/addons/treeview.html
it toggle Tablacus Explorer Tree (left sidebar)

[REQUIRED] Backtick for comma escaping:
https://www.autohotkey.com/docs/commands/_EscapeChar.htm

[WARNING] Blank Scoop shim window will be opened:
https://github.com/lukesampson/scoop/pull/3998
https://github.com/lukesampson/scoop/issues/1606
https://github.com/lukesampson/scoop-extras/issues/2801
*/
Run TE "D:\EricsBooks" "Tabs`,Close other tabs" "D:\SashaProgramsStorage\SashaAudiosPhotosAndVideos\ShareX\KiraTriviador"
OutputDebug Run Tablacus Explorer


/*
[ACTION] Open most used directories in ConEmu + Far

[LEARN][CONEMU] Command-line arguments;
“/” and “-” are equivalent:
https://conemu.github.io/en/ConEmuArgs.html

[LEARN][CONEMU] “-Dir” — set working (startup) directory for ConEmu and console processes

[LEARN][CONEMU] “-runlist” — run several tabs is startup;
it renovated variant of “-cmdlist”.

[INFO] Use “|||” delimiter in “.ahk” files, but “^|^|^|” in Windows command prompt:
https://superuser.com/a/1448967/572069
*/
Run conemu64 -runlist far -Dir "D:\EricsBooks" ||| far -Dir "D:\EricsBooks\Серии\Аванта +\Фирменные энциклопедии" ||| far -Dir "D:\Mega.co.nz\Джинджерины\EricsRooms\EricsRoomsPackages"
OutputDebug Run ConEmu


#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
﻿/*
[ACTION] Close some apps before stream.

[PURPOSE] They:
1. May contain personal data — Ferdi, Thunderbird, qBittorrent, Xtreme Download Manager
2. That have unexpected tabs and used on the stream
(then I open the same apps with expected tabs) — ConEmu, SumatraPDF, Tablacus Explorer
*/


/*
[ACTION] Close ConEmu

[NOTE] Adding “ConEmu64”, “ConEmu” and “Far” to the list below not solve this problem,
so I use taskkill

[NOTE] I can't find command-line argument for ConEmu closing:
https://conemu.github.io/en/ConEmuArgs.html
*/
KiraKillAppAndTree("ConEmu64")


/*
[LEARN][AUTOHOTKEY] Lists usage:
https://www.autohotkey.com/docs/Objects.htm#Usage_Simple_Arrays
https://www.autohotkey.com/boards/viewtopic.php?t=34990#p162624

[REQUIRED] Double quotes for string values, no single quotes or non-quotes;
quotes not needed for integer values

[WARNING] Comments inside lists not supported

[INFO] “thunderbird” kill “ThunderbirdPortable.exe” and “thunderbird.exe” both;
but closing “ThunderbirdPortable.exe” doesn't kill “thunderbird.exe”

[INFO] Put ConEmu, Tablacus Explorer and SumatraPDF to beginning,
because these apps will be used on streams
*/
KiraList := ["TE", "SumatraPDF", "qbittorrent", "ferdi", "thunderbird"]

KiraLoop("KiraCloseNonStreamProcesses", KiraList)


/*
[ACTION] Close XDM Download manager window:
https://subhra74.github.io/xdm/

[INFO] XDM have “javaw.exe” “.exe” file;
another software may have the same “exe”

[LEARN][AUTOHOTKEY] Get Process ID number of xdman to close it:
https://stackoverflow.com/a/19159923/5951529
https://www.computerhope.com/jargon/p/pid.htm
https://www.autohotkey.com/docs/commands/WinGet.htm#PID
*/
WinGet, KiraXDManID, PID, ahk_class SunAwtFrame ahk_exe javaw.exe

/*
[LEARN][AUTOHOTKEY] Close window by Process Identifier:
https://www.autohotkey.com/docs/commands/Process.htm#Parameters
*/
Process, Close, %KiraXDManID%
OutputDebug Close Xtreme Download Manager, PID %KiraXDManID%


#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateKillTree.ahk

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return
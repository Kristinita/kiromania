# 1. Summary

Set of AutoHotkey scripts, which used for streaming on [**Kiromania**](https://dlive.tv/Kiromania) ([**OrnithopteraAlexandrae**](https://www.twitch.tv/ornithopteraalexandrae) on Twitch) channel.

# 2. Scripts

1. **KiraMain.ahk** — main file, that contains shortcuts to run scripts
1. **KiraBeforeStream**— to go from normal PC workplace to stream workplace
1. **KiraAfterStream** — to return from stream to normal PC workplace
1. **KiraGeneral** — scripts, used both before and after the stream
1. **KiraStream** — hotkeys, that used in the streaming process
1. **KiraTemplates** — templates files

Details you can see in the files of the scripts.

# 3. Limitations

## 3.1. Windows

As of July 2020 AutoHotkey works for Windows solely. Kiromania scripts will not work on Linux, macOS or another operational systems.

## 3.2. Apps

I wrote Kiromania scripts for apps, that I personally use. Examples:

1. Streaming software — [**OBS Studio**](https://obsproject.com/)
1. Browser — [**Firefox**](https://www.mozilla.org/en-US/firefox)
1. Text editor — [**Sublime Text**](https://www.sublimetext.com/)
1. Books viewer — [**Sumatra PDF**](https://www.sumatrapdfreader.org/free-pdf-reader.html)

and so on. Your environment will be different from mine.

# 4. Environment variables

For using Kiromania scripts you need to set `PATH_KIROMANIA` environment variable with value — a path to root of your Kiromania repository.

Also, in Kiromania I use environment variables to specify paths for my software.

# 5. Tips

You can use as [**Open URL Sublime Text plugin**](https://packagecontrol.io/packages/Open%20URL) for quick navigation between Kiromania scripts. Add next lines to your `User\open_url.sublime-settings` file:

```jsonc
/*
[PACKAGE] “Open URL” — open local URLs:
https://packagecontrol.io/packages/Open%20URL
*/
{
    /*
    [INFO] Aliases for paths:
    https://github.com/noahcoad/open-url/issues/49#issuecomment-490070075
    */
    "aliases": {
        "%A_ScriptDir%": ".",
        "%PATH_KIROMANIA%": "D:\\KiraGitLab\\Kiromania"
    },

    /*
    [INFO] URLs delimiters;
    «Quotes» required for me:
    https://github.com/noahcoad/open-url#settings
    */
    "delimiters": " \t\n\r\"'`,*<>[**](){} “”«»",

    /*
    [INFO] Open file at the specific line:
    https://github.com/noahcoad/open-url/blob/734ce5f8e5a119badf1ac866c10b3d9124f08ff2/open_url.sublime-settings#L92-L100
    */
    "other_custom_commands": [
      {
        "label": "OpenURL: Open file at specific line",
        "pattern": ":[0-9]+$",
        "commands": ["subl"],
        /*
        [INFO] Directory of currently opened file:
        https://github.com/noahcoad/open-url#setc-wd-directory-for-shell-command
        */
        "kwargs": { "cwd": "current_file" }
      }
    ]
}
```

# 6. Licences

[**Licenses for images, that used in scripts**](https://gitlab.com/search?project_id=19943900&scope=blobs&search=%5BLICENSE%5D).

# 7. Acknowledgments

The name of the project, all names of files, functions, variables etc. of this repository is named after [**Kira Kenyukhova**](https://vk.com/psychologist_kira_k) — the Greatest Goddess of All Time.

![Kira Eleison](https://i.imgur.com/KGBGZHe.jpg)
; [ACTION] Revert font size for all Sublime Text windows to default

/*
[LEARN][SUBLIMETEXT] Execute the given Sublime Text command:
https://sublime-text-unofficial-documentation.readthedocs.io/en/latest/command_line/command_line.html#options

[REQUIRED] RevertFontSize Sublime Text plugin:
https://packagecontrol.io/packages/RevertFontSize
*/
Run subl --command revert_font_size

return
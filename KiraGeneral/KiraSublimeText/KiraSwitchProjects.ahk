/*
[PURPOSE] Template for switching between Sublime Text projects

[ACTION] Script open new Sublime Text project (KiraFirstProject) in new window,
then close previous project (KiraSecondProject) window
*/

KiraSwitchSublimeTextProjects(KiraFirstProject, KiraSecondProject)
{
	/*
	[LEARN][SUBLIMETEXT] Open Sublime Text project from command line:
	https://sublime-text-unofficial-documentation.readthedocs.io/en/latest/command_line/command_line.html#options
	https://forum.sublimetext.com/t/opening-a-project-from-command-line/2531/3?u=sasha_chernykh

	[NOTE] I couldn't find, how I can get via command-line behavior as for “Project Manager” Sublime Text package:
	“Ctrl+Shift+P” → “Project Manager” → “Open Project” → select expected project

	[NOTE] Don't use “-a” option for this command!
	It add workplace from expected project to current project;
	you can get strange results.
	*/
	Run subl --project "%SUBLIME%\Data\Packages\User\Projects\%KiraFirstProject%.sublime-project"
	OutputDebug Run subl --project "%SUBLIME%\Data\Packages\User\Projects\%KiraFirstProject%.sublime-project"

	Sleep 4000

	/*
	[NOTE] OBS Studio can't recognize window by part of the title:
	https://ideas.obsproject.com/posts/847/support-wildcards-in-window-capture-source
	https://obsproject.com/forum/threads/window-match-priority-what-does-it-do.35195/#post-337255
	It may activate Sublime Text window for another project.
	Therefore, solely one project window must be activated on the stream

	[REQUIRED] nircmd:
	https://www.nirsoft.net/utils/nircmd.html
	[NOTE] I couldn't find any open source programs with the same behavior instead of nircmd

	[LEARN][NIRCMD] It close the window (not kill the task),
	that have “(%KiraSecondProject%) - Sublime Text” in the title:
	https://nircmd.nirsoft.net/win.html
	https://stackoverflow.com/a/32739482/5951529
	*/
	Run nircmd win close ititle (%KiraSecondProject%) - Sublime Text
	OutputDebug Run nircmd win close ititle (%KiraSecondProject%) - Sublime Text
}

return

﻿; [ACTION] Close all Firefox tabs


/*
[REQUIRED] “Show tab previews in the Windows taskbar” Firefox setting:
https://www.groovypost.com/tips/enable-firefox-previews-windows-taskbar/
[REQUIRED] “false” value for “browser.tabs.closeWindowWithLastTab” in Firefox “about:config”:
https://www.howtogeek.com/264880/how-to-prevent-firefox-from-exiting-when-you-close-the-last-tab/
*/


; [INFO] “MozillaTaskbarPreviewClass” — ahk_class for Firefox previewed tabs in “explorer.exe”
KiraAHKClass = MozillaTaskbarPreviewClass


/*
[LEARN][AUTOTHOTKEY] Pass variable across scripts:
https://autohotkey.com/board/topic/46711-how-to-pass-variables-from-one-script-to-another/?p=290921
*/
Run %PATH_KIROMANIA%\KiraTemplates\KiraTemplateCloseAllTabs.ahk %KiraAHKClass%

return
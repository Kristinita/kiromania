/*
[ACTION] Open URL in new window without toolbars

[PURPOSE] I use “Window Capture” sources in my OBS Studio.
See arguments in 8.3 item of my issue:
https://github.com/obsproject/obs-studio/issues/2554#issue-586079148
I want to remove all extra elements from Firefox window,
so that it has the solely game area.
*/


/*
[REQUIRED] “New window without toolbar” Firefox addon:
https://addons.mozilla.org/en-US/firefox/addon/new-window-without-toolbar/
https://github.com/tkrkt/NewWindowWithoutToolbar


[NOTE] I couldn't find any alternative method:

1. “Fileresizer” (https://directedge.us/content/firesizer-firefox-extension),
“RKiosk” (https://stackoverflow.com/a/33769886/5951529), “Hide Caption Titlebar Plus” and
“Hide Navigation Bar” (https://stackoverflow.com/a/37286202/5951529) addons
doesn't work for Firefox Quantum
2. Command-line arguments “-width” and “-heigth”
(http://kb.mozillazine.org/Command_line_arguments#List_of_command_line_arguments_.28incomplete.29)
doesn't work in new Firefox versions:
https://developer.mozilla.org/en-US/docs/Mozilla/Command_Line_Options
3. “firefox -new-window --kiosk https://russia.triviador.net/”
(https://www.ghacks.net/2019/10/07/firefox-71-new-kiosk-mode-for-the-browser/) run Firefox
in fullscreen mode. I can't set specific Firefox window parameters.
4. Cmdow “cmdow /T” and “cmdow 0x2B10736 /siz 400 600 /act” doesn't resize Firefox fullscreen window:
https://ritchielawrence.github.io/cmdow/
5. “Windowed” addon doesn't work for Flash (Triviador is a Flash game):
https://addons.mozilla.org/en-US/firefox/addon/windowed/


[INFO] URLs of “Svoya igra”, “Brain Ring”, “What? Where? When?” is different every time,
so streamer must open them manually

[INFO] I use “Windowed” addon for full window, not full screen videos
from sites as PeerTube or YouTube
*/


/*
[WARNING] I couldn't find, how I can run commands from third-party Firefox addons
via command line like in programs as Sublime Text or Tablacus Explorer.
Therefore, I use AutoHotkey for run addon command.
*/
WinActivate ahk_class MozillaWindowClass ahk_exe firefox.exe


/*
[INFO] “Shift+Alt+N” — shortcut for “New window without toolbar” actions

[INFO] Check “Close the current page when opening the window” option
in “New window without toolbar”, that prevent duplicate windows
*/
Send !+n
OutputDebug Open MozillaDialogClass new window

return
; [PURPOSE] Template for open/close Firefox tabs before and after stream


/*
[REQUIRED] Additional variable

I can't add “1”, “"1"” or “%1%” to list
*/
KiraOpenTabs = %1%


KiraList := ["KiraGeneral\KiraFirefox\KiraCloseAllTabs", KiraOpenTabs]

KiraLoop("KiraRunAHKScripts", KiraList)

#Include %A_ScriptDir%\..\..\KiraTemplates\KiraTemplateLoop.ahk

return

﻿/*
[DEPRECATED]
1. Bookmarks folders names always closed for me. It's normally, not uncomfortable.
2. Compact-toolbar addon has “toggle” shortcut solely, not “hide/show” separate shortcuts.
Undesirable behavior may occur.

[ACTION] Hide Firefox bookmarks folders names

[PURPOSE] So that stream users doesn't see them
*/


/*
[REQUIRED] compact-toolbar Firefox extension required:
https://addons.mozilla.org/en-US/firefox/addon/compact-toolbar/

[INFO] “Ctrl+Shift+y” — default compact-toolbar shortcut for toggle:
https://github.com/kakakikikeke/compact-toolbar/issues/3#issuecomment-609513888
*/
Send ^+y

return
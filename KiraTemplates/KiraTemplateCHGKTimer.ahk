; [PURPOSE] Template for CHGK timers


/*
[REQUIRED] “Sacagawea” Sublime Text package:
https://kristinita.netlify.app/sublime-text/sacagawea

[NOTE] I can't found any open source timer, so I use my own.
Hourglass issue for example:
https://github.com/dziemborowicz/hourglass/issues/193

[REQUIRED] Manual stopping YouTube (or another) video player by press “k” key.
When I want to run any Sacagawea timer, YouTube player may be active or stopped.
I can't found, how I can force stop YouTube player in any case.

[REQUIRED] Active Text-To-Speech. Web Speech API demonstration page
have solely toggle button, but not separate buttons for start and stop Text-To-Speech.
*/
KiraCHGKTimer(KiraTimerCommand)
{
	Run %PATH_KIROMANIA%\KiraStream\KiraWhatWhereWhen\KiraToggleSpeechToText.ahk
	OutputDebug Stop Speech-To-Text

	/*
	[INFO] Sacagawea commands:
	https://github.com/Kristinita/Sacagawea/blob/SashaDevelop/Sacagawea.py
	*/
	Run subl --command sacagawea_%KiraTimerCommand% -b
	OutputDebug Run sacagawea_%KiraTimerCommand%
}
; [PURPOSE] Switching between specific scenes in OBS Studio


/*
[INFO] “WinActivate” — focused on specific window:
https://www.autohotkey.com/docs/commands/WinActivate.htm

[LEARN][AUTOHOTKEY] Use Window Spy for “ahk_id” get:
https://amourspirit.github.io/AutoHotkey-Snippit/WindowSpy.html
https://autohotkey.com/board/topic/63365-using-window-spy/?p=399361

[LEARN][AUTOHOTKEY] ahk_id — name of executable file:
https://www.autohotkey.com/docs/misc/WinTitle.htm#ahk_id
*/


/*
[LEARN][AUTOHOTKEY] Functions Optional parameters:
https://www.autohotkey.com/docs/Functions.htm#optional

[LEARN][AUTOHOTKEY] Empty value for optional parameter:
https://stackoverflow.com/a/12701444/5951529
*/


/*
[INFO]

1. “KiraAHKExeName” — “ahk_exe” of the app, that must be activated, without extension;
2. “KiraAHKClass” — its “ahk_class”;
3. “KiraAHKExeExtension” — “ahk_exe” extension

[INFO] Values of these parameters — variables from “KiraSwitchToScene” function
*/
KiraActivate(KiraAHKExeName, KiraAHKClass, KiraAHKExeExtension="exe")
{
	/*
	[REQUIRED] No spaces between variables;

	[NOTREQUIRED] "Double quotes"
	*/
	WinActivate ahk_class %KiraAHKClass% ahk_exe %KiraAHKExeName%.%KiraAHKExeExtension%
	OutputDebug Activate ahk_class %KiraAHKClass% ahk_exe %KiraAHKExeName%.%KiraAHKExeExtension%
}


/*
[INFO]

1. “KiraFirstApp” = “KiraAHKExeName”
2. “KiraFirstAppClass” = “KiraAHKClass”
3. “KiraRestream” — run Restream Chat or no
4. “KiraAHKExeExtension” = “KiraExtension”
*/
KiraSwitchToScene(KiraFirstApp, KiraFirstAppClass, KiraRestream="", KiraExtension="exe")
{
	; [REQUIRED] Sleep; otherwise scenes in OBS Studio will not switch
	Sleep 400

	If %KiraRestream%
		{
			KiraActivate("Restream Chat", "Chrome_WidgetWin_1")
			OutputDebug Activate Restream Chat
		}

	/*
	[INFO] If we need to activate 2 applications in different parts of the screen;
	“KiraSecondApp” additional parameter value may be required
		If %KiraSecondApp%
			{
				KiraActivate(KiraSecondApp, KiraSecondAppClass)
			}
	*/
	KiraActivate(KiraFirstApp, KiraFirstAppClass, KiraExtension)
}

return
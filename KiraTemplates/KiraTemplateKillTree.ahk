; [PURPOSE] Kill program tree by “taskkill” — default Windows app

/*
[LEARN][TASKKILL] Kill process by specific name:
https://ss64.com/nt/taskkill.html
https://www.computerhope.com/taskkill.htm
“/T” argument — Kill tree; ConEmu have tree of processes

[REQUIRED] “/F” — force close for “ConEmu64.exe”
*/

KiraKillAppAndTree(KiraAppExe)

{
	Run taskkill /IM %KiraAppExe%.exe /T /F
	OutputDebug Close %KiraAppExe%.exe process with subprocesses forced
}

return
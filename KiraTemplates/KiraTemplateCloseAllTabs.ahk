﻿; [PURPOSE] Template for closing all tabs, include current and pinned, for program


/*
[LICENSE] user3419297 — author of this script:
https://superuser.com/a/1537613/572069
*/


/*
[REQUIRED] “Hidden windows are included only if
"DetectHiddenWindows" has been turned on.”:
https://www.autohotkey.com/docs/commands/DetectHiddenWindows.htm
https://www.autohotkey.com/docs/commands/WinGet.htm#List
*/
DetectHiddenWindows 1


/*
[LEARN][AUTOHOTKEY] Each array must be initialized before use:
https://www.autohotkey.com/docs/misc/Arrays.htm#object-based
*/
KiraArray := []


/*
[LEARN][AUTOHOTKEY] “WinGet” for lists:
https://www.autohotkey.com/docs/commands/WinGet.htm#List

[INFO] “KiraNumberOfElements” — number of opened elements, include current and pinned:
Firefox — tabs;
Windows Explorer — windows.

[LEARN][AUTOHOTKEY] “%1%” — ahk_class:
https://www.autohotkey.com/docs/misc/WinTitle.htm#ahk_class

[INFO] Check “Follow Mouse” in AutoSpy, that get ahk_class of app tabs in Windows taskbar
*/
WinGet, KiraNumberOfElements, List, ahk_class %1%


/*
[LEARN][AUTOHOTKEY] Object-based arrays:
https://www.autohotkey.com/docs/misc/Arrays.htm#object-based

[LEARN][AUTOHOTKEY] Count — how many iterations to perform the loop:
https://www.autohotkey.com/docs/commands/Loop.htm#parameters

[NOTE] I use single loop instead of solution from GEV answer,
because extra variable in For-Loop array:
https://superuser.com/a/1537613/572069
*/
Loop %KiraNumberOfElements% {

	/*
	[LEARN][AUTOHOTKEY] “A_Index” — number of current iteration:
	https://www.autohotkey.com/docs/Variables.htm#loop

	[NOTE] GEV AutoHotkey user says, that “KiraNumberOfElements%A_Index%”
	is not anti-pattern:
	https://superuser.com/a/1537613/572069
	*/
    KiraArray.Push(KiraNumberOfElements%A_Index%)

	/*
	[LEARN][WINDOWS] HWND — window handle:
    https://stackoverflow.com/a/18040503/5951529
    https://www.autohotkey.com/docs/commands/ControlGet.htm#Parameters
	*/
	KiraHWND := KiraArray[A_Index]

	/*
	[LEARN][AUTOHOTKEY] “ahk_id” — short handle to window:
	https://www.autohotkey.com/docs/misc/WinTitle.htm#ahk_id
	*/
	WinClose ahk_id %KiraHWND%
}

return

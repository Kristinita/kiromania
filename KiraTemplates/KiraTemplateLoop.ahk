; [PURPOSE] Loop template

/*
[LEARN][AUTOHOTKEY] Count in loop:
https://www.autohotkey.com/docs/commands/Loop.htm#Parameters
in my case it — number of item in “
”

[INFO] “KiraCommands” — commands, that will be runned
“KiraList” — array

[INFO] “KiraList” in template script must be the same variable name
as in the scripts, that we must to run

[WARNING] “KiraList” required in the function
although it have the same value “KiraList” for all scripts

[WARNING] Extra variable “Key” required for For-Loop arrays as in Super User answer,
it not optional; so I use Loop instead of For-Loop:
https://www.autohotkey.com/docs/commands/For.htm
https://superuser.com/a/1537613/572069
*/
KiraLoop(KiraCommands, KiraList)
{
	/*
	[LEARN][AUTOHOTKEY] List length:
	https://www.autohotkey.com/docs/objects/Object.htm#Length
	*/
	KiraItemsNumber := KiraList.Length()

	Loop %KiraItemsNumber%
	{
		/*
		[LEARN][AUTOHOTKEY] Get list item for specific index:
		https://www.autohotkey.com/boards/viewtopic.php?t=34990#p162624
		*/
		KiraListItem := KiraList[A_Index]

		/*
		[LEARN][AUTOHOTKEY] “if/elif”:
		https://www.autohotkey.com/docs/commands/IfExpression.htm
		https://www.autohotkey.com/boards/viewtopic.php?t=30321#p142073
		*/

		; [INFO] Run stream web pages
		If (KiraCommands = "KiraRunWebPages")
		{
			Run https://%KiraListItem%
			OutputDebug Run https://%KiraListItem%
		}

		; [INFO] Close non-stream processes
		else if (KiraCommands = "KiraCloseNonStreamProcesses")
		{
			/*
			[LEARN][AUTOHOTKEY] Process, Close:
			https://www.autohotkey.com/docs/commands/Process.htm#Close

			[LEARN][AUTOHOTKEY] “WinClose” close window, but not kill process:
			https://www.autohotkey.com/docs/commands/WinClose.htm
			*/
			Process, Close, %KiraListItem%.exe
			OutputDebug Close %KiraListItem%.exe
		}

		; [INFO] Open some programs for streaming and another after stream
		else if (KiraCommands = "KiraOpenApps")
		{
			Run %KiraListItem%
			OutputDebug Run %KiraListItem%
		}

		; [INFO] Close AutoHotkey scripts
		else if (KiraCommands = "KiraCloseAHKScripts")
		{
			/*
			[LEARN][AUTOHOTKEY] Kill AutoHotkey scripts from a different script:
			https://autohotkey.com/board/topic/94333-kill-an-ahk-script-from-a-different-script/?p=594354
			*/
			WinClose %KiraListItem%.ahk
			OutputDebug Close %KiraListItem%.ahk
		}

		; [INFO] Run AutoHotkey scripts
		else if (KiraCommands = "KiraRunAHKScripts")
		{
			Run %PATH_KIROMANIA%\%KiraListItem%.ahk
			OutputDebug Run %KiraListItem%.ahk
		}

		; [INFO] Run notes files
		else if (KiraCommands = "KiraOpenNotes")
		{
			/*
			[LEARN][SUBLIMETEXT] Open a file via command-line:
			https://stackoverflow.com/a/47345081/5951529
			*/
			Run subl %PATH_GINGERINAS_NOTES%\%KiraListItem%.note
			OutputDebug Open %PATH_GINGERINAS_NOTES%\%KiraListItem%.note
		}

		/*
		[INFO] If “1400”, I have unexpected behavior in these cases:
		1. “%PATH_KIROMANIA%\KiraBeforeStream\KiraFirefox\KiraFirefoxStreamingTabs.ahk” and “%PATH_KIROMANIA%\KiraAfterStream\KiraFirefox\KiraFirefoxEverydayTabs.ahk” — incorrect tabs order
		2. “%PATH_KIROMANIA%\KiraGeneral\KiraFirefox\KiraOpenURLInNewWindow.ahk” — open blank window
		*/
		Sleep 2000
	}
}

return
